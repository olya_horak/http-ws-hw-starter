export const texts = [
  "Tip for a tidy desk - Use a tray for any papers you have to read. Have a jar for pens and pencils and have a bin near your desk.",
  "Tip for a tidy desk - Go to the kitchen when you want to have a drink or, if you prefer, keep a drink on a small table near your desk.",
  "Tip for a tidy desk - Try to keep as much information as possible in folders on your computer. Before printing a document, ask yourself, ‘do I really need to read this on paper?",
  "Tip for a tidy desk - If you have a lot of paper (magazine articles, notes, worksheets, etc.), use a scanner and keep a digital version as a PDF on your computer.",
  "Tip for a tidy desk - For example, take photos of notes to yourself, the name and address of a place you need to visit or diagrams you need to study for school.",
  "Tip for a tidy desk - If you really do need to keep small bits of paper, use a noticeboard on the wall. Check it every day and throw old notes in the bin.",
  "Tip for a tidy desk - Choose a time to tidy your desk and do it! If you do it every day, it will only take five minutes and you can start each new day with a clean and tidy space.",
];

export default { texts };
